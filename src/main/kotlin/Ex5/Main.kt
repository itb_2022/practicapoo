package Ex5

import java.util.*

fun main() {

    var armMechanicState = false // Comença el programa amb el braç apagat
    val armMechanic = ArmMechanic(false, 0.0f, 0.0f)
    val scan = Scanner(System.`in`)

    println(" ▶ Apreta el botó de la màquina ")
    while (armMechanic.armMechanicState == false) {
        println("Vols encendre la màquina?")
        var turnOnTurnOff = scan.next().uppercase()
        if (turnOnTurnOff == "SI"){
            armMechanic.armMechanicState = armMechanic.toggle(armMechanicState)
        }else if (turnOnTurnOff == "NO"){
            armMechanic.armMechanicState = false
        }

    }

    while (armMechanic.armMechanicState == true){
        println(" ▶ Quina acció vols dur a terme?")
        println("  Vols cambiar l'altura del braç? Escriu '1'")
        println("  Vols cambiar l'angle del braç? Escriu '2'")
        var moveArm1or2 = scan.nextInt()

        when(moveArm1or2){
            1 ->{
                println("Quina altura vols que es mogui? Introdueix els CM que el vols moure (Recorda que te que anar de 0 a 30cm)")
                var heightArm = scan.nextFloat()
                while (heightArm < 0 || heightArm > 30){
                    println("Torna a introduir correctament l'altura (Recorda que te que anar de 0 a 30cm)")
                    heightArm = scan.nextFloat()
                }
                armMechanic.heightArm = armMechanic.updateAltitude(heightArm)
                println("Altura actual del braç :" + armMechanic.heightArm)
            }
            2 ->{
                println("Quina angle vols que es mogui? Introdueix els Angles que vols moure (Recorda que te que anar de 0 a 360º)")
                var angleArm = scan.nextFloat()
                while (angleArm < 0 || angleArm > 360){
                    println("Torna a introduir correctament l'angle (Recorda que te que anar de 0 a 360º)")
                    angleArm = scan.nextFloat()
                }
                armMechanic.angleArm = armMechanic.updateAngle(angleArm)
                println("Angle actual del braç :"+ armMechanic.angleArm)
            }
        }
    }
}

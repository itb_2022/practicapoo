package Ex5

class ArmMechanic(b: Boolean, fl: Float, fl1: Float) {

    var armMechanicState: Boolean = b
    var angleArm: Float = fl
    var heightArm: Float = fl1

    fun toggle(armMechanicState: Boolean): Boolean{
        var state = true
        if (armMechanicState){
            state = false
            return state
        } else return state

    }

    fun updateAltitude(heightArm: Float): Float{
        var newHeightArm = heightArm
        return newHeightArm
    }

    fun updateAngle(angleArm: Float): Float{
        var newAngleArm = angleArm
        return newAngleArm
    }
}
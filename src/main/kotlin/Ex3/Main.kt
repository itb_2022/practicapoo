package Ex3

import java.util.*

fun main() {

        var numberOfElements = mutableListOf<Int>()
        var dataTaulells = mutableListOf<Taulell>()
        var dataLlistons = mutableListOf<Llisto>()
        val scan = Scanner(System.`in`)
        print(" ▶ Escriu quants taulells vols: ")
        var taulells = scan.nextInt()
        numberOfElements.add(0,taulells)
        var elementsTaulell = numberOfElements[0]

        if (elementsTaulell>0) {
            for (i in 0..elementsTaulell-1) {
                print(" ▶ Escriu el preu unitari/m² del taulell: ")
                var priceTaulell = scan.nextFloat()
                print(" ▶ Escriu la llargada del taulell (en CM): ")
                var llargadaTaulell = scan.nextFloat()
                print(" ▶ Escriu la amplada del taulell (en CM): ")
                var ampladaTaulell = scan.nextFloat()
                dataTaulells.add(Taulell(priceTaulell, llargadaTaulell, ampladaTaulell))
            }
        }

        var pricesTaulells = mutableListOf<Float>()
        for (i in 0..dataTaulells.size-1){
            var price = Taulells().priceOfTaulells(dataTaulells[i])
            pricesTaulells.add(price)
        }

        print(" ▶ Escriu quants llistons vols: ")
        var llistons = scan.nextInt()
        numberOfElements.add(1,llistons)
        var elementsLlisto = numberOfElements[1]

        if (elementsLlisto>0){
            for (i in 0..elementsLlisto-1){
                print(" ▶ Escriu el preu unitari del taulell: ")
                var priceLlisto = scan.nextFloat()
                print(" ▶ Escriu la llargada del taulell (en CM): ")
                var llargadaLlisto = scan.nextFloat()
                dataLlistons.add(Llisto(priceLlisto,llargadaLlisto))
            }
        }

        var pricesLlistons = mutableListOf<Float>()
        for (i in 0..dataLlistons.size-1){
            var price = Llistons().priceOfLlistons(dataLlistons[i])
            pricesLlistons.add(price)
        }

    println(" Preu Taulells : ${pricesTaulells.sum()}€ \n Preu Llistons : ${pricesLlistons.sum()}€ ")
}
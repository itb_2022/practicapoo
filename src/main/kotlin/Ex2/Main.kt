package Ex2

import Ex1.Pasta
import java.util.*

fun main() {

    var acabar = false
    var ensucradaBeguda: Boolean
    var llistaDePastes = mutableListOf<Pasta>()
    var llistaDeBegudes = mutableListOf<Beguda>()
    while (acabar == false){
        val scan = Scanner(System.`in`)
        println(" ▶ Escriu el nom de la pasta i la beguda ")
        var nomPasta = scan.next().uppercase()
        var nomBeguda = scan.next().uppercase()
        println(" ▶ Escriu el preu, pes i calories de la pasta")
        var preuPasta = scan.nextFloat()
        var pesPasta = scan.nextFloat()
        var caloriesPasta = scan.nextFloat()
        llistaDePastes.add(Pasta(nomPasta,preuPasta,pesPasta,caloriesPasta))

        println(" ▶ Escriu el preu")
        var preuBeguda = scan.nextFloat()
        println(" ▶ És ensucrada?")
        var ensucradaBegudaNoBoolean = scan.next().uppercase()
        if (ensucradaBegudaNoBoolean == "SI" || ensucradaBegudaNoBoolean == "ENSUCRADA"){
            ensucradaBeguda = true
        } else
            ensucradaBeguda = false

        llistaDeBegudes.add(Beguda(nomBeguda,preuBeguda,ensucradaBeguda))

        if (llistaDePastes.size == 3 && llistaDeBegudes.size == 3 ){
            acabar = true
        }

    }
    print(llistaDePastes)
    print(llistaDeBegudes)


}
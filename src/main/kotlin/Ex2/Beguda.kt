package Ex2

data class Beguda(var nom:String, var preu:Float, var ensucrada:Boolean){
    init {
        if (ensucrada == true){
            preu = preu + (preu/10)
        }
    }
}

import Ex1.Pasta
import java.util.*

fun main() {

    var acabar = false
    var llistaDePastes = mutableListOf<Pasta>()
    while (acabar == false){
        val scan = Scanner(System.`in`)
        println(" ▶ Escriu el nom de la pasta ")
        var nomPasta = scan.next().uppercase()
        println(" ▶ Escriu el preu, pes i calories ")
        var preuPasta = scan.nextFloat()
        var pesPasta = scan.nextFloat()
        var caloriesPasta = scan.nextFloat()
        llistaDePastes.add(Pasta(nomPasta,preuPasta,pesPasta,caloriesPasta))

        if (llistaDePastes.size == 3){
            acabar = true
        }

    }
    print(llistaDePastes)


}